<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ChangePasswordRequest;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;
use App\User;

class ChangePasswordController extends Controller
{
    public function process(ChangePasswordRequest $request){
      return $this->getPasswordResetRow($request)->count()>0 ? $this->changePassword($request) : $this->tokenNotFoundResponse();
      // return $request;
    }

    private function getPasswordResetRow($request){
      return DB::table('password_resets')->where(['email'=>$request->email, 'token'=> $request->resetToken]);
    }

    private function changePassword($request){
      $user = User::whereEmail($request->email)->first();
      $user->update(['password'=>$request->password]);
      $this->getPasswordResetRow($request)->delete();
      return response()->json(['data' => 'El password se ah guardado correctamente'], Response::HTTP_CREATED);
    }
    private function tokenNotFoundResponse(){
      return response()->json(['error'=>'Token o correo electronico son incorrectos'], Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
