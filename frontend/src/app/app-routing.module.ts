import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { ProfileComponent } from './profile/profile.component';
import { SingupComponent } from './singup/singup.component';
import { PasswordResetComponent } from './password-reset/password-reset.component';
import { PasswordRResetComponent } from './password-r-reset/password-r-reset.component';
import { BeforeLoginService } from './services/before-login.service';
import { AfterLoginService } from './services/after-login.service';


const appRoutes: Routes = [
	{ path: 'login',component: LoginComponent, canActivate: [BeforeLoginService]},
	{ path: 'singup',component: SingupComponent, canActivate: [BeforeLoginService]},
	{ path: 'profile',component: ProfileComponent, canActivate: [AfterLoginService]},
	{ path: 'request-password-reset',component: PasswordResetComponent, canActivate: [BeforeLoginService]},
	{ path: 'response-password-reset',component: PasswordRResetComponent, canActivate: [BeforeLoginService]},

];

@NgModule({
  imports: [
  	 RouterModule.forRoot(appRoutes)
  ],
  exports:[RouterModule]
})
export class AppRoutingModule { }
