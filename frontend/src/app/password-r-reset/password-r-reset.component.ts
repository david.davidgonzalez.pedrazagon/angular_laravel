import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JarwisService } from '../services/jarwis.service';

import { Router } from '@angular/router';
import { SnotifyService } from 'ng-snotify';

@Component({
  selector: 'app-password-r-reset',
  templateUrl: './password-r-reset.component.html',
  styleUrls: ['./password-r-reset.component.css']
})
export class PasswordRResetComponent implements OnInit {

	public error = [];
	public form = {
			email: null,
			password: null,
			password_confirmation:null,
			resetToken: null,
	
		};
  constructor(
  	private route: ActivatedRoute,
    private jarwis: JarwisService,
    private router: Router,
    private Notify: SnotifyService
  	) {
  		route.queryParams.subscribe(params => {
  			this.form.resetToken = params['token']
  			
  		});
  }

  onSubmit(){
    this.jarwis.changePassword(this.form).subscribe(
      data=> this.handleResponse(data),
      error => this.handleError(error)
      )
  }

  handleResponse(data){
    let _router = this.router;
    this.Notify.confirm('Done, New login with new password', {
      buttons: [
        {
          text: 'Okay',
          action: toster => { _router.navigateByUrl('/login'),
            this.Notify.remove(toster.id)
          }
        },
       ]
    });
    
  }

  handleError(error){
    this.error = error.error.errors;
  }

  ngOnInit() {
  }

}
