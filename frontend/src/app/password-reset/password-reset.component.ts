import { Component, OnInit } from '@angular/core';
import { JarwisService } from '../services/jarwis.service';
import { SnotifyService } from 'ng-snotify';

@Component({
  selector: 'app-password-reset',
  templateUrl: './password-reset.component.html',
  styleUrls: ['./password-reset.component.css']
})
export class PasswordResetComponent implements OnInit {
	public form = {
		email: null
	};

  public error = null;

  constructor(
   private jarwis: JarwisService,
   private notify: SnotifyService
    ) { }

  ngOnInit() {
  }
  
  onSubmit(){
    this.notify.info("Esperando...",{timeout:5000});
  	this.jarwis.sendResetPassworddata(this.form).subscribe(
  		data => this.handleResponse(data),
  		error => this.notify.error(error.error.error)
  	);
  }
  handleResponse(res){
  	this.notify.success(res.data,{timeout:0});
  	this.form.email = null;
  }

}
