import { Component, OnInit } from '@angular/core';
// import { HttpClient } from '@angular/common/http';
import { JarwisService } from '../services/jarwis.service';
import { TokenService } from '../services/token.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-singup',
  templateUrl: './singup.component.html',
  styleUrls: ['./singup.component.css']
})
export class SingupComponent implements OnInit {

	public form = {
		email: null,
		name: null,
		password: null,
		password_confirmation: null,
	}

	public error = [];

  constructor( 
  	private Jarwis: JarwisService,
    private Token : TokenService,
    private router: Router,
  	) { }


	onSubmit(){
		this.Jarwis.signup(this.form).subscribe(
			data => this.handleResponse(data),
			error => this.handleError(error),
			);
	}

	handleResponse(data){
	    this.Token.handle(data.access_token);
	    this.router.navigateByUrl('/profile')
  	}

	handleError(error){
	  	this.error = error.error.errors;
	}


  ngOnInit() {
  }

}
 