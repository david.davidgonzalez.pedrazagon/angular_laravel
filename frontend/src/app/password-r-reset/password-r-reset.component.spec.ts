import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PasswordRResetComponent } from './password-r-reset.component';

describe('PasswordRResetComponent', () => {
  let component: PasswordRResetComponent;
  let fixture: ComponentFixture<PasswordRResetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PasswordRResetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PasswordRResetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
